
package br.com.casa.ex7;

public class Calculadora {
    public static final String APROVADO = "Aprovado";
    public static final String RECUPERACAO = "Recuperacao";
    public static final String REPROVADO = "Reprovado";
    
    public String calcular (double nota){
        
        if(nota >= 7){
            return APROVADO;
        }
        if(nota < 7 && nota >=4){
            return RECUPERACAO;
        }
        
        return REPROVADO;
    }
    
    
}
